<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

$pizas = [
    [
        'id' => 1,
        'imagem' => 'http://www.picanhacia.com.br/wp-content/uploads/2017/01/11379225_1180312101994735_933388139_n-13.jpg',
        'sabor' => 'CARNE DE SOL COM CATUPIRY',
        'valor' => 'R$30,00',
        'tamanho' => 'G',
        'ingredientes' => 'Carne de sol desfiada e temperada, azeitona, cebola, catupiry'
    ],
    [
        'id' => 2,
        'imagem' => 'http://www.pizzaexpressdelivery.com/fotos/HAVAIANA.jpg',
        'sabor' => 'HAVAIAN',
        'valor' => 'R$25,00',
        'tamanho' => 'M',
        'ingredientes' => ''
    ],
    [
        'id' => 3,
        'imagem' => 'https://craftlog.com/m/i/1563829=s1280=h960',
        'sabor' => 'QUATRO QUEIJOS',
        'valor' => 'R$30,00',
        'tamanho' => 'M',
        'ingredientes' => ''
    ],
    [
        'id' => 4,
        'imagem' => 'https://t1.rg.ltmcdn.com/pt/images/9/8/3/img_pizza_calabresa_e_mussarela_4389_600.jpg',
        'sabor' => 'CALABRESA',
        'valor' => 'R$22,00',
        'tamanho' => 'G',
        'ingredientes' => ''
    ]
];

if( isset($_GET['id']) ){
    $id = $_GET['id'];

    foreach( $pizas as $pizza ){
        if( $pizza['id'] == $id ){
        print_r(json_encode($pizza));
        }
    }
} else {
    print_r(json_encode($pizas));
}
?>