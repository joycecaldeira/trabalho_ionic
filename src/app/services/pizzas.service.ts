import { Injectable } from '@angular/core';

import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PizzasService {

  public destroySubscription$: Subject<void> = new Subject();
  
  constructor(private http: HttpClient) { }


  getPizzas() : Observable<any[]> {
    return this.http.get<any[]>('http://localhost/api_pizzas/index.php');
  }

  getPizzasId( id: number ){
    let params = new HttpParams().set('id', id.toString());

    return this.http.get<any>('http://localhost/api_pizzas/index.php',{ params });
  }
}
