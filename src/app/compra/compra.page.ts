import { Component, OnInit } from '@angular/core';
import { PizzasService } from '../services/pizzas.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.page.html',
  styleUrls: ['./compra.page.scss'],
})
export class CompraPage implements OnInit {
  public pizza: any;
  public id: number;
  constructor(private servicesPizzas: PizzasService,
    public loadingController: LoadingController,
    private route: ActivatedRoute,
    public alertController: AlertController) {
    
   }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    if( this.id != null ){
      this.servicesPizzas.getPizzasId(this.id)
      .subscribe( data => {
        this.pizza = data;
      })
    }
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Terminando',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    this.presentAlert();
  } 

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Pedido confirmado',
      message: 'Aguarde um pouco',
      buttons: ['OK']
    });

    await alert.present();
  }
}
