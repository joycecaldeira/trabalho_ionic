import { Component } from '@angular/core';
import { PizzasService } from '../services/pizzas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public pizzas: any[];

  constructor(private servicesPizzas: PizzasService,private router: Router) {
    
    this.servicesPizzas.getPizzas()
    .subscribe( data => {
      this.pizzas = data;
    });

  }

  pizza( id: number ){
    this.router.navigate(['pizza', id]);
  }
}
