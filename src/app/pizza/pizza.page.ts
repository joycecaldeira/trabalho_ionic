import { Component, OnInit } from '@angular/core';
import { PizzasService } from '../services/pizzas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pizza',
  templateUrl: './pizza.page.html',
  styleUrls: ['./pizza.page.scss'],
})
export class PizzaPage implements OnInit {
  public pizza: any;
  public id: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private servicesPizzas: PizzasService
    ) {

   }

  ngOnInit() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    if( this.id != null ){
      this.servicesPizzas.getPizzasId(this.id)
      .subscribe( data => {
        console.log(data)
        this.pizza = data;
      })
    }
  }

  pedirPizza( id: number){
    this.router.navigate(['compra', id])
  }

}
